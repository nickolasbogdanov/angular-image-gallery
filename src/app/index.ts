import * as angular from 'angular';
import './app.module';

angular.element(document).ready(() => {
    angular.bootstrap(document, [
        'app'
    ]);
});
