import {GalleryController} from './gallery.controller';

export class Gallery implements ng.IComponentOptions {

    public controller: Function;
    public templateUrl: string;
    public controllerAs: string;
    public bindings: any;

    constructor() {
        this.controller = GalleryController;
        this.controllerAs = 'vm';
        this.templateUrl = './dist/gallery/gallery.partial.html';
        this.bindings = {
            itemsPerPage: '@',
            images: '<'
        };
    }
}

