/* tslint:disable:no-string-literal */
import {IImagesService, IImagesData} from '../services/image.service';

export class GalleryController {
    public images: IImagesData[];
    public currentImage: number;
    public pages: number[];
    public itemsPerPage: number;
    public currentPage: number;
    public copyOfData: IImagesData[];
    public imageName: string;
    public sliceItems: number;
    public imagesPerPage: number;
    public optionsForSelectList: string[];
    public selectedOption: string;

    constructor(
        private imagesService: IImagesService,
        private $stateParams: ng.ui.IStateParamsService,
        private $state: ng.ui.IStateService,
        private $compile: ng.ICompileService,
        private $scope: ng.IScope
    ) {
        if (this.images) {
            this.currentPage = $stateParams['page'] || 1;
            this.getImagesRelativeToCurrentPage();
            this.optionsForSelectList = ['Items per Page', '5', '10', '15', '20'];
            this.selectedOption = $stateParams['itemsPerPage'] || 'Items per Page';
            this.imagesPerPage = $stateParams['itemsPerPage'] || this.itemsPerPage || 10;
            this.pages = this.createPagination(this.images.length, this.imagesPerPage);
        }
    }

    public changeItemsPerPage(): void {
        let selectedOptions: number = parseInt(this.selectedOption, 10);
        if (!isNaN(selectedOptions)) {
            this.imagesPerPage = selectedOptions;
            this.pages = this.createPagination(this.copyOfData.length, selectedOptions);
            this.$state.go('gallery', { itemsPerPage: selectedOptions });
        }
    }

    public prevPage(): void {
        this.$state.go('gallery', { page: --this.currentPage });
    }

    public nextPage(): void {
        this.$state.go('gallery', { page: ++this.currentPage });
    }

    public createColorbox(index: number): void {
        angular.element(document.body).append(
            this.$compile(`<colorbox images='vm.copyOfData' index='${index + this.sliceItems}'><colorbox>`)(this.$scope)
        );
    }

    private getImagesRelativeToCurrentPage(): void {
        let spliced: IImagesData[];
        this.sliceItems = this.itemsPerPage * (this.currentPage - 1);
        spliced = this.images.splice(0, this.sliceItems);
        this.copyOfData = spliced.concat(this.images);
    }

    private createPagination(itemsCount: number, itemsPerPage: number): number[] {
        if (!itemsPerPage) {
            return;
        }

        let result: number[] = new Array(Math.ceil(itemsCount / itemsPerPage));

        for (let i: number = 0, x: number = result.length; i < x; i++) {
            result[i] = i + 1;
        }

        return result;
    }

}
