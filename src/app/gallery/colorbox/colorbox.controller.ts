import {IImagesData} from '../../services/image.service';

export class ColorboxController {
    public index: number;
    public images: IImagesData[];

    constructor() {

        document.addEventListener('keydown', function removeColorbox(e: KeyboardEvent): void {
            if (e.keyCode === 27) {
                this.closeColorbox();
                document.removeEventListener('keydown', removeColorbox);
            }
        }.bind(this));
    }

    public prevImage(): void {
        if (this.index > 0) {
            --this.index;
        }
    }

    public nextImage(): void {
        if (this.index < this.images.length) {
            ++this.index;
        }
    }

    public closeColorbox(): void {
        angular.element(document.getElementsByTagName('colorbox')).remove();
    }

}
