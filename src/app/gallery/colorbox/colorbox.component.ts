import {ColorboxController} from './colorbox.controller';

export class Colorbox implements ng.IComponentOptions {

    public controller: Function;
    public templateUrl: string;
    public controllerAs: string;
    public bindings: any;

    constructor() {
        this.controller = ColorboxController;
        this.controllerAs = 'vm';
        this.templateUrl = './dist/gallery/colorbox/colorbox.partial.html';
        this.bindings = {
            images: '<',
            index: '@',
        };
    }
}

