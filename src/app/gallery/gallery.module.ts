import * as angular from 'angular';
import {Gallery} from './gallery.component';
import {Colorbox} from './colorbox/colorbox.component';

import './gallery.css!';
import './colorbox/colorbox.css!';

const app: ng.IModule = angular.module('app.gallery', []);

app.component('gallery', new Gallery);
app.component('colorbox', new Colorbox);
