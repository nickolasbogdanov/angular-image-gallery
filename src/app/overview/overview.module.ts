import * as angular from 'angular';
import 'angular-ui-router';

import {OverviewRouteConfig} from './overview.route';
import {OverviewController} from './overview.controller';

const overview: ng.IModule = angular.module('app.overview', ['ui.router']);

overview.controller('OverviewController', OverviewController);
overview.config(OverviewRouteConfig);

