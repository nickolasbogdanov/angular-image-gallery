import {IImagesService, IImagesData} from '../services/image.service';

export class OverviewController {
    public images: IImagesData[];
    public currentImage: number;
    public pages: number[];
    public itemsPerPage: number;

    constructor(private imagesService: IImagesService) {
        imagesService.getImages().then((images: ng.IHttpPromiseCallbackArg<IImagesData[]>) => {
            this.images = images.data;
        }).catch((error: Error) => {
            console.error(error);
        });
    }
}
