export function OverviewRouteConfig($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider): void {
    $stateProvider.state('overview', {
        url: '/',
        controller: 'OverviewController',
        controllerAs: 'vm',
        templateUrl: 'dist/overview/overview.partial.html'
    });

    $stateProvider.state('gallery', {
        url: '/gallery?page&itemsPerPage',
        controller: 'OverviewController',
        controllerAs: 'vm',
        templateUrl: 'dist/examples/gallery.html'
    });
    $urlRouterProvider.otherwise('/');
}
