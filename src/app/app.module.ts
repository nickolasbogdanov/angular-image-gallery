/// <reference path="../../typings/browser.d.ts" />
import * as angular from 'angular';

import './services/services.module';
import './gallery/gallery.module';
import './overview/overview.module';

angular.module('app', [
    'app.services',
    'app.overview',
    'app.gallery'
]);
