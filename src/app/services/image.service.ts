export interface IImagesData {
    url: string;
    date: string;
    title: string;
}

export interface IImagesService {
    getImages(): ng.IPromise<IImagesData[]>;
}

export class ImagesService implements IImagesService {

    constructor(private $http: ng.IHttpService) { }

    getImages(): ng.IPromise<IImagesData[]> {
        return this.$http.get('./src/app/data/images.json');
    }
}
