import * as angular from 'angular';
import {ImagesService} from './image.service';

const services: ng.IModule = angular.module('app.services', []);

services.service('imagesService', ImagesService);
