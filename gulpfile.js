var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var path = require('path');
var config = {
    projectDir: __dirname,
    tsDir: path.join(__dirname, 'src', 'app'),
    publicDir: path.join(__dirname, 'dist'),
};
var cp = require('child_process');

// Static server
gulp.task('serve', ['scripts', 'views', 'styles'], function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('scripts', function() {
    var tscJs = path.join(process.cwd(), 'node_modules', 'typescript', 'bin', 'tsc');
    var childProcess = cp.spawn('node', [tscJs, '-p', config.tsDir], { cwd: process.cwd() });

    childProcess.stdout.on('data', function(data) { console.log(data.toString()) });
    childProcess.stderr.on('data', function(data) { console.log(data.toString()) });

});

gulp.task('views', function() {
    gulp.src(config.tsDir + '/**/*.html')
        .pipe(gulp.dest(config.publicDir));
});

gulp.task('styles', function() {
    gulp.src(config.tsDir + '/**/*.css')
        .pipe(gulp.dest(config.publicDir));
});


gulp.task('watch', ['scripts', 'views', 'styles'], function() {
    gulp.watch('./src/app/**/*.ts', ['scripts']);
    gulp.watch('./src/**/*.ts', ['scripts']);
    gulp.watch('./src/app/**/*.html', ['views']);
    gulp.watch('./src/app/**/*.css', ['styles']);
});
