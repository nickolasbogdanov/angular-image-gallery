# angular simple image gallery
In order to start project run.
```
npm start
```
Then in new window of terminal run
```
gulp serve
```

Or you can manually run all commands in order to install dependencies.
```
npm install
jspm install
typings install
gulp watch
gulp serve
```

### Technology Stack

* TypeScript 
* Angular 1.5.x
* JSPM 
* Gulp 

### Requirements
-------
* nodejs 4.+
* npm 3.+


### Gulp tasks
-------

Compiling options

```
gulp styles       # move all css files
gulp views        # moves all templates to the serving folder
gulp scripts      # compile typescript to javascript
gulp watch        # compile and watch ts/views/styles files
```